students = []


class Student:
    school_name = "Springfield Elementary"  # static variable

    def __init__(self, name, student_id=332):  # constructor
        self.name = name  # instance variable
        self.student_id = student_id
        students.append(self)

    def __str__(self):  # think java toString()
        return "Student " + self.name

    def get_name_capitalize(self):
        return self.name.capitalize()

    def get_school_name(self):
        return self.school_name


class HighSchoolStudent(Student):  # same as extends Student in java
    school_name = "Springfield High School"

    def get_school_name(self):
        return "This is a high school student"

    def get_name_capitalize(self):
        original_value = super().get_name_capitalize()
        return original_value + "-HS"


james = HighSchoolStudent("james")
print(james.get_name_capitalize())

# Note: No access modifiers -- everything is public, underscore as first character of method often used to
#    indicate the method should not be accessed directly or overridden
#   -No override keyword
#   -No interfaces

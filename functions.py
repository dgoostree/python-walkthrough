students = []


def get_students_titlecase():
    students_titlecase = []
    for student in students:
        students_titlecase.append(student["name"].title())
    return students_titlecase


def print_students_titlecase():
    students_titlecase = get_students_titlecase()
    print(students_titlecase)


# =332 assigns default if parameter omitted
def add_student(name, student_id=332):
    student = {"name": name, "student_id": student_id}
    students.append(student)


#variable argumentsask
def var_args(name, *args):
    print(name)
    print(args)


#args as dictionary
def keyword_args(name, **args):
    print(name)
    print(args["description"], args["feedback"])


#equivalent statements
#add_student("Mark", 15)
#add_student(name="Mark", student_id=15)

#var_args("Mark", False, 15)

#keyword_args("Mark", description="Descrip text", feedback=None, attending=True)

#read input from console

while input("Add a student? ").lower() == "yes":
    student_name = input("Enter student name: ")
    student_id = input("Enter student id: ")
    add_student(student_name, student_id)

print_students_titlecase()


#can define nested functions
def get_students():
    students = ["mark", "james"]
    def get_students_titlecase():
        students_titlecase = []
        for student in students:   #can access variable defined outside
            students_titlecase.append(student.title())
        return students_titlecase
    students_titlecase_names = get_students_titlecase()
    print(students_titlecase_names)


get_students()

# lambda function - second statement samme as defined function
# either is invoked the same way: double(5)
def double(x):
    return x * 2


double = lambda x: x * 2

print("Hello World!")

# This is a comment

# No type declaration needed,
answer = 42
name = "Python"


# no type verification in editor, but type hinting can help:
def add_numbers(a: int, b: int) -> int:
    return a + b


print("Add numbers: " + str(add_numbers(8, 5)))

# type casting
answer = 42
pi = 3.14159

int(pi) == 3
float(answer) == 42.0
str(3) == "3"

# strings
'Hello World' == "Hello World" == """Hello World"""
"hello".capitalize() == "Hello"
"hello".replace("e", "a") == "hallo"
"hello".isalpha() == True
"123".isdigit() == True
"some, csv, values".split(", ") == ["some", "csv", "values"]

# string format function
name = "PythonBo"
machine = "HAL"
"Nice to meet you {0}, I am {1}".format(name, machine)

# string interpolation also possible
print(f"Nice to meet you {name}, I am {machine}")
# string slicing referenced but not explained

# booleans and none
python_course = True
java_course = False

# booleans convert to ints like c
int(python_course) == 1
int(java_course) == 0
str(python_course) == "True"

# None like null in java
aliens_found = None

# if syntax, is keyword mentioned: used to determine if two variables point to the same object in memory
number = 5
if number == 5:
    print("Number is 5")
else:
    print("Number is not 5")

# truthy and falsy values: anything other than 0 or empty string is true.
# boolean False and None value are false
# for lists, same as null checking list object (if list)

# != and if not
number = 5
if number != 5:
    print("This will not execute")

python_course = True
if not python_course:
    print("This will not execute")

# use words and and or for multi condition ifs
if number == 3 and python_course:
    print("Do stuff")

if number == 17 or python_course:
    print("Do stuff")

# ternary if statements
a = 1
b = 2
"bigger" if a > b else "smaller"


# lists -- can contain objects of different types
student_names = []
student_names = ["Mark", "Katarina", "Jessica"]
student_names[0] == "Mark"
student_names [2] = "Jessica"
student_names[-1] = "Jessica"  # weird

student_names [0] = "James"  # replaces mark in the list

# add to list at end
student_names.append("Homer")

# check if list contains item
"Mark" in student_names == True  # mark is there

# length
len(student_names) == 4

# delete from list
del student_names[2]  # bye Jessica you basic bitch

# slice - skip items in list and get the rest.  does NOT modify the list
student_names = ["Mark", "Katarina", "Jessica"]
student_names[1:] == ["Katarina", "Jessica"]
student_names[1:-1] == ["Katarina"]  # first and last sliced
# list comprehension mentioned as being powerful

# loops - for -- think java for each
for name in student_names:
    print("Student name is {0}".format(name))

# range function -- range(10) executes from 0 to 9
x = 0
for index in range(10):
    x += 10
    print("The value of X is {0}".format(x))
# can also call like range(5, 10) -- starts at 5 and goes through nine
# can also call like range(5, 10, 2) -- 2 is the increment, so [5, 7, 9]

# break and continue statements works like you'd expect

# while -- boolean expression or True
while x < 10:
    print("Do stuff")
    x += 1

#
# Dictionaries - key value pairs of data.  Similar to json
# keys and values can be any type
student = {
    "name": "Mark",
    "student_id": 15163,
    "feedback": None
}

# can be nested: key of another dictionary

#list of dictionaries
all_students = [
    {"name": "Mark", "student_id": 15163},
    {"..."}
]

# retrieve a value
student["name"] == "Mark"
student["last_name"] == KeyError  # exception

# retrieve some default if last name is not defined (so no exception)
student.get("last_name", "Unknown") == "Unknown"

# list of all keys
student.keys()

# list of all values
student.values()

#delete key value pair from dictionary
del student["name"]


#
# Exceptions
last_name = student["last_name"] # whoops, exception

try:
    last_name = student["last_name"]
except KeyError:
    print("Error finding last_name")
except TypeError as error:
    print("Do stuff")
    print(error)  # error message displayed natively by python
except Exception:
    print("All other exceptions")

# kk
student = {
    "name": "Mark",
    "student_id": 15163,
    "feedback": None
}

student["last_name"] = "Kowalski"


# complex type for complex numbers
# long type exists in python 2 but replaced by int in python 3
# bytes and bytearray
# tuple = (3, 5, 1, "Mark")  -- immutable
# set and frozenset
set([3, 2, 3, 1, 5]) == (1, 2, 3, 5)

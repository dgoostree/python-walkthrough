# import hs_student  -- requires hs_student. prefix to use HighSchoolStudent
# from hs_student import HighSchoolStudent  -- imports HighSchoolStudent, prefix no longer needed
from hs_student import *  # get all from hs_student

james = HighSchoolStudent("james")
print(james.get_name_capitalize())

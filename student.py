students = []


class Student:
    school_name = "Springfield Elementary"  # static variable

    def __init__(self, name, student_id=332):  # constructor

        """
        This is similar to javadoc.  This is the constructor of the class.
        :param name: string - student name
        :param student_id: integer - optional student ID
        """

        self.name = name  # instance variable
        self.student_id = student_id
        students.append(self)

    def __str__(self):  # think java toString()
        return "Student " + self.name

    def get_name_capitalize(self):
        return self.name.capitalize()

    def get_school_name(self):
        return self.school_name

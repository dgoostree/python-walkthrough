# pip is python's package manager -- included automatically with python 2 and up
# PyPI website is an index of available python packages
# 'pip3 install flask' installs flask package from within the project's venv (/Script folder)
from flask import Flask
app = Flask(__name__)


@app.route("/")
def hello():
    return "Hello World!"


if __name__ == "__main__":
    app.run()

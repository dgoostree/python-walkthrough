#Generator functions - Yield
students = []


def read_file():
    try:
        f = open("students.txt", "r")
        for student in read_students(f):  # iterate over results of file iteration
            students.append(student)
        f.close()
    except Exception:
        print("Could not read file")


def read_students(f):
    for line in f:
        yield line  # iterate over all lines in file


read_file()
print(students)
